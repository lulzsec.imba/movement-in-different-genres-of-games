﻿using UnityEngine;
using UnityEngine.AI;
public class ClickToMove : MonoBehaviour
{
    public Animator animator;
    public NavMeshAgent agent;
    public Transform destination;

    void Start()
    {
        animator = GetComponent<Animator>();
        agent = GetComponent<NavMeshAgent>();
    }

    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            RaycastHit hit;
            if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hit, 100))
            {
                agent.SetDestination(hit.point);
                animator.SetBool("Walking", true);
            }
        }
        if(agent.remainingDistance <= agent.stoppingDistance)
        {
            animator.SetBool("Walking", false);
        }
    }
}
