﻿
using UnityEngine;

public class CattoMovement : MonoBehaviour
{
    public Rigidbody2D rigidbody2D;
    public Animator cattoAnimator;

    public bool cattoIsFacingRight = false;
    public bool cattoIsGrounded = false;
    public bool cattoIsJumping = false;

    public Transform groundedCheck;
    public float groundedCheckRadius;
    public LayerMask ground;
    public float input;
    public float cattoSpeed;
    public float cattoJumpForce;

    void Start()
    {
        rigidbody2D = GetComponent<Rigidbody2D>();
        cattoAnimator = GetComponent<Animator>();
    }

    void Update()
    {
        cattoIsGrounded = Physics2D.OverlapCircle(groundedCheck.position, groundedCheckRadius, ground);
        input = Input.GetAxis("Horizontal");

        if (cattoIsGrounded)
        {
            cattoAnimator.SetFloat("Velocity", Mathf.Abs(input));
        }

        if(Input.GetButtonDown("Jump") && cattoIsGrounded)
        {
            cattoIsJumping = true;
            cattoAnimator.SetTrigger("Jump");
        }

        if(Input.GetKeyDown(KeyCode.DownArrow) && cattoIsGrounded)
        {
            cattoAnimator.SetBool("Crouch", true);
        }
        else if (Input.GetKeyDown(KeyCode.UpArrow))
        {
            cattoAnimator.SetBool("Crouch", false);
        }
    }

    private void FixedUpdate()
    {
        rigidbody2D.velocity = new Vector2(input * cattoSpeed, rigidbody2D.velocity.y);
        if(cattoIsFacingRight == false && input > 0)
        {
            FlipCatto();
        }
        if (cattoIsFacingRight == true && input < 0)
        {
            FlipCatto();
        }
        if (cattoIsJumping)
        {
            rigidbody2D.AddForce(new Vector2(0f, cattoJumpForce));

            cattoIsJumping = false;
        }
    }

    private void FlipCatto()
    {
        cattoIsFacingRight = !cattoIsFacingRight;
        Vector3 cattoScale = transform.localScale;
        cattoScale.x *= -1;
        transform.localScale = cattoScale;
    }
}
