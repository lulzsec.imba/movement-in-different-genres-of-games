﻿using UnityEngine;

public class TopDownMovement : MonoBehaviour
{
    public int velocity;
    public Vector3 movement;
    public Animator animator;


    void Start()
    {
        animator = GetComponent<Animator>();

    }

    void Update()
    {
        movement = new Vector3(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"));

        animator.SetFloat("Horizontal", movement.x);
        animator.SetFloat("Vertical", movement.y);
        animator.SetFloat("Amount", movement.magnitude);
        transform.position += movement * velocity * Time.deltaTime;
    }
}